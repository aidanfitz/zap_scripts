# This is a script for ZAP and should be run before a different test script such as Robot Framework.
# Configures the settings for a scan before it happens.
from zapv2 import ZAPv2
from pprint import pprint
import time

apikey = 'e0r1h1ag1nh0r1o85iegfufu7d'


# Creating the ZAP object for API calls.
# Setting it with specific API key and allowing the port to be 8090 for both https and http.
zap = ZAPv2(apikey=apikey, proxies={
    'http': 'http://localhost:8090',
    'https': 'http://localhost:8090'})


# Using 'exclude_from_proxy' API option to exclude all URL which are not starting with 'http(s)://int1-www.petco.com.*'
zap.core.exclude_from_proxy(apikey=apikey, regex="^(?:(?!https?:\/\/int1-www.petco.com).*).$")
print('Setup complete')