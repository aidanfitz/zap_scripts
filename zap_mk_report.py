from zapv2 import ZAPv2
from pprint import pprint
import time

# Configuration settings

apikey = 'e0r1h1ag1nh0r1o85iegfufu7d'
shutdownOnceFinished = True



# Creating the ZAP object for API calls.
# Setting it with specific API key and allowing the port to be 8090 for both https and http.
zap = ZAPv2(apikey=apikey, proxies={
    'http': 'http://localhost:8090',
    'https': 'http://localhost:8090'})


# Specifies a file location to save a report, then use API call 'htmlreport()' to write that file.
print('Making HTML Report')
f2 = open('/var/lib/jenkins/workspace/zap_api_enabled/reports/zap_report' + str(time.time()) + '.html','w')
f2.write(zap.core.htmlreport(apikey=apikey))
f2.close()