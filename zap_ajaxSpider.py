# Script to both configure and run ZAP using AJAX Spider.

from zapv2 import ZAPv2
from pprint import pprint
import time

# Setting variables for target address and apikey to be used for ZAP API. 
target = 'http://int1-www.petco.com/shop/en/petcostore'
apikey = 'e0r1h1ag1nh0r1o85iegfufu7d'
useAjaxSpider = True
shutdownOnceFinished = True

# Creating the ZAP object for API calls.
# Setting it with specific API key and allowing the port to be 8090 for both https and http.
zap = ZAPv2(apikey=apikey, proxies={
    'http': 'http://localhost:8090',
    'https': 'http://localhost:8090'})


# Using 'exclude_from_proxy' API option to exclude all URL which are not starting with 'https://int1-www.petco.com.*'
zap.core.exclude_from_proxy(apikey=apikey, regex="^(?:(?!https?:\/\/int1-www.petco.com).*).$")

zap.core.access_url(target, followredirects=True, apikey=apikey)
time.sleep(5)


# Ajax Spider Settings are configured before scan.
ajax = zap.ajaxSpider
ajax.set_option_reload_wait(100, apikey=apikey)
ajax.set_option_event_wait(100, apikey=apikey)
ajax.set_option_max_duration(2, apikey=apikey)



# Start AJAX Spider Scan, and wait until it finishes or until maximum time is reached.
print('Initiating Spider {}'.format(target))
ajax.scan(target, apikey=apikey)
time.sleep(10)
while (ajax.status != 'stopped'):
    print('Ajax Spider is ' + ajax.status)
    time.sleep(10)

print('Spider is now complete.')
time.sleep(2)

print('Attempting HTML Report')
f2 = open('/var/lib/jenkins/workspace/ajax_spider_int1/reports/zap_report' + str(time.time()) + '.html','w')
f2.write(zap.core.htmlreport())
f2.close()
